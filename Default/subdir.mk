################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../cfglib.c \
../config.c \
../error.c \
../main.c \
../pipe.c \
../sio.c \
../sock.c \
../string.c \
../thread.c \
../vlist.c 

O_SRCS += \
../main.o \
../sio.o 

OBJS += \
./cfglib.o \
./config.o \
./error.o \
./main.o \
./pipe.o \
./sio.o \
./sock.o \
./string.o \
./thread.o \
./vlist.o 

C_DEPS += \
./cfglib.d \
./config.d \
./error.d \
./main.d \
./pipe.d \
./sio.d \
./sock.d \
./string.d \
./thread.d \
./vlist.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -D__WIN32__ -DSIO_WIN32 -DWINTHREADS -DSOCK_WIN -I"D:\MinGW64\include\dbus-1.0" -I"D:\MinGW64\lib\dbus-1.0\include" -O2 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


