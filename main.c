/*
 * main.c
 *
 * main module for serproxy
 *
 * (C)1999 Stefano Busti
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dbus/dbus.h>

#if defined(__UNIX__)
#  include <unistd.h>
#  include <fcntl.h>
#  include <sys/time.h>
#elif defined(__WIN32__)
#  include <windows.h>
#  include <io.h>
#include <stdbool.h>
#endif

#include "sio.h"
#include "sock.h"
#include "pipe.h"
#include "thread.h"
#include "vlist.h"
#include "cfglib.h"
#include "config.h"
#include "error.h"

int readcfg(void);
void cleanup(void);
int waitclients(void);
thr_startfunc_t serve_pipe(void *data);
void debug(void);
int dbus_receive_init();

#if defined(__UNIX__)
char cfgfile[] = "/etc/serproxy.cfg";
#define LOCKFILE "/var/run/serproxy.pid"
#define LOCKMODE (S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH)
#elif defined(__WIN32__)
char cfgfile[] = "serproxy.cfg";
#define LOCKFILE "serproxy.pid"
FILETIME ftLastWriteTime;
#endif
DBusConnection* conn;
cfg_s cfg;
vlist_s pipes;

#if defined(__UNIX__)
int already_running(void)
{
    int     fd;
    char    buf[16];

    fd = open(LOCKFILE, O_RDWR|O_CREAT, LOCKMODE);
    if (fd < 0) {
        syslog(LOG_ERR, "can't open %s: %s", LOCKFILE, strerror(errno));
        exit(1);
    }
    if (lockfile(fd) < 0) {
        if (errno == EACCES || errno == EAGAIN) {
            close(fd);
            return(1);
        }
        syslog(LOG_ERR, "can't lock %s: %s", LOCKFILE, strerror(errno));
        exit(1);
    }
    ftruncate(fd, 0);
    sprintf(buf, "%ld", (long)getpid());
    write(fd, buf, strlen(buf)+1);
    return(0);
}
#endif


int main(int argc, char **argv)
{

		if (sock_start())
		return -1;
	vlist_init(&pipes, pipe_destroy);
	cfg_init(&cfg, 0);
	atexit(cleanup);
	readcfg();
#ifdef DEBUG
	debug();
#endif
	dbus_receive_init();
	waitclients();
	
	return 0;
}

void cleanup(void)
{
	cfg_cleanup(&cfg);
	vlist_cleanup(&pipes);
	sock_finish();
}

int readcfg(void)
{
	char ports[BUFSIZ], *p;
	int port;
	pipe_s *pipe;
	cfg_s local;
	serialinfo_s sinfo;
	char *parity;
#if defined(__UNIX__)
	struct stat sb;
#elif defined(__WIN32__)
	WIN32_FILE_ATTRIBUTE_DATA sb;
#endif
	printf("Reading config\n");

	ZeroMemory(&sb,sizeof(WIN32_FILE_ATTRIBUTE_DATA));
	GetFileAttributesEx(cfgfile,GetFileExInfoStandard,&sb);
	ftLastWriteTime=sb.ftLastWriteTime;
	

	/* Read the global config settings */
	cfg_fromfile(&cfg, cfgfile);
	
	/* Read the comm port list */
	if (cfg_readbuf(cfgfile, "comm_ports", ports, sizeof(ports)) == NULL)
		errend("Couldn't find 'comm_ports' entry in config file '%s'", cfgfile);

	vlist_clear(&pipes);

	/* Parse the comm ports list */
	p = strtok(ports, ",");
	while (p)
	{
		if (sscanf(p, "%d", &port) > 0)
		{
			pipe = malloc(sizeof(pipe_s));
			//pipe_init(pipe);
			if (pipe == NULL)
				perrend("malloc(pipe_s)");

			cfg_init(&local, port);
			
			/* Copy global settings to those for current pipe */
			cfg_assign(&local, &cfg);

			/* Set the comm port */
			local.ints[CFG_IPORT].val = port;

			/* Load this pipe's config */
			cfg_fromfile(&local, cfgfile);

			/* Try initializing the pipe */
			if (pipe_init(pipe, local.ints[CFG_INETPORT].val))
				perrend("pipe_init");

			/* Copy over the rest of the pipe's config */
			pipe->timeout = local.ints[CFG_ITIMEOUT].val;
			sinfo.port = port;
			sinfo.baud = local.ints[CFG_IBAUD].val;
			sinfo.stopbits = local.ints[CFG_ISTOP].val;
			sinfo.databits = local.ints[CFG_IDATA].val;

			parity = local.strs[CFG_SPARITY].val;

			if (strcmp(parity, "none") == 0)
			{
				sinfo.parity = SIO_PARITY_NONE;
			}
			else if (strcmp(parity, "even") == 0)
			{
				sinfo.parity = SIO_PARITY_EVEN;
			}
			else if (strcmp(parity, "odd") == 0)
			{
				sinfo.parity = SIO_PARITY_ODD;
			}
			else
			{
				errend("Unknown parity string '%s'", parity);
			}

			if (sio_setinfo(&pipe->sio, &sinfo))
				errend("Unable to configure comm port %d", port);
			
			/* Finally add the pipe to the pipes list */
			vlist_add(&pipes, pipes.tail, pipe);

			cfg_cleanup(&local);
		}
		
		p = strtok(NULL, ",");
	}

	/* Clean up local cfg struct */
	cfg_cleanup(&local);
	
	return 0;
}

int waitclients(void)
{
	int fd_max;
	fd_set fds;
	vlist_i *it;
	pipe_s *pit, *newpipe;
	tcpsock_s *newsock;
	thr_t thread;
	struct timeval tv;
	tv.tv_sec=10;
	fd_max = 0;
	int select_return;
	//int count=0;
#if defined(__UNIX__)
	struct stat sb;
#elif defined(__WIN32__)
	WIN32_FILE_ATTRIBUTE_DATA sb;
#endif


	FD_ZERO(&fds);

	/* Set all sockets to listen */
	for (it = pipes.head; it; it = it->next)
	{
		pit = (pipe_s *)it->data;

		if (tcp_listen(&pit->sock))
			perror("waitclients() - tcp_listen()");
	}

	printf("Serproxy - (C)1999 Stefano Busti - Waiting for clients\n");
	
	while (1)
	{



		/* Iterate through the pipe list */
		for (it = pipes.head; it; it = it->next)
		{
			pit = (pipe_s *)it->data;

			/* Monitor socket fd of each */
			FD_SET(pit->sock.fd, &fds);

			/* Track max fd */
			if (pit->sock.fd > fd_max)
				fd_max = pit->sock.fd;
		}

		/* Wait for a read ( == accept() in this case) */
		if((select_return=select(fd_max + 1, &fds, NULL, NULL, &tv)) == -1)perrend("waitclients() - select()");

		#if defined(__UNIX__)
		stat(cfgfile,&buf);
		#elif defined(__WIN32__)
		ZeroMemory(&sb,sizeof(WIN32_FILE_ATTRIBUTE_DATA));
		GetFileAttributesEx(cfgfile,GetFileExInfoStandard,&sb);
		if(CompareFileTime(&sb.ftLastWriteTime,&ftLastWriteTime)!=0)readcfg();
		#endif


	//	count++;


		fprintf(stderr, ".%d",select_return);

		/* Find which sockets dare involved */
		for (it = pipes.head; it; it = it->next)
		{

			pit = (pipe_s *)it->data;

			dbus_listen(); //TODO if any changes thr_cancel(pit->thread);


			/*
			thr_mutex_trylock(pit->mutex);
			pit->count++;
			thr_mutex_unlock(pit->mutex);
			*/



			if (FD_ISSET(pit->sock.fd, &fds))
			{
				/* Create a new pipe struct for the new thread */
				newpipe = malloc(sizeof(pipe_s));
				if (!newpipe)
					perrend("waitclients() - malloc(pipe_s)");

				newpipe->sio = pit->sio;

				/* Try to open serial port */
				if (sio_open(&newpipe->sio))
				{
					tcp_refuse(&pit->sock);
					error("Failed to open comm port - connection refused");
					free(newpipe);
					continue;
				}
				
				/* Accept the connection */
				newsock = tcp_accept(&pit->sock);

				/* All ok? */
				if (newsock)
				{
					newpipe->sock = *newsock;
					free(newsock);

					newpipe->timeout = pit->timeout;
					newpipe->mutex = pit->mutex;
						
					/* Create the server thread */
					if (thr_create(&thread, 1, serve_pipe, newpipe))
					{
						error("Feck - thread creation failed");
						free(newpipe);
					}
					else
					{
					//	newpipe->thread=thread;
						pit->thread=thread;
						fprintf(stderr, "Server thread launched\n");
					}
				}
				else
				{
					perror("waitclients() - accept()");
					free(newpipe);
				}
			}
		}
	}
	return 0;
}

int reply_to_method_call(DBusMessage* msg, DBusConnection* conn)
{
   DBusMessage* reply;
   DBusMessageIter args;
   int stat = 1;
   dbus_uint32_t level = 21614;
   dbus_uint32_t serial = 0;
   char* param = "";

   printf("right interface & method\n");

   // read the arguments
   if (!dbus_message_iter_init(msg, &args))
      fprintf(stderr, "Message has no arguments!\n");
   else if (DBUS_TYPE_STRING != dbus_message_iter_get_arg_type(&args))
      fprintf(stderr, "Argument is not string!\n");
   else
      dbus_message_iter_get_basic(&args, &param);

   printf("Method called with %s\n", param);

   // create a reply from the message
   reply = dbus_message_new_method_return(msg);


   // add the arguments to the reply
   dbus_message_iter_init_append(reply, &args);
   if (!dbus_message_iter_append_basic(&args, DBUS_TYPE_BOOLEAN, &stat)) {
      fprintf(stderr, "Out Of Memory!\n");
     return -1;
   }
   if (!dbus_message_iter_append_basic(&args, DBUS_TYPE_UINT32, &level)) {
      fprintf(stderr, "Out Of Memory!\n");
      return -1;
   }

   // send the reply && flush the connection
   if (!dbus_connection_send(conn, reply, &serial)) {
      fprintf(stderr, "Out Of Memory!\n");
      return -1;
   }

   dbus_connection_flush(conn);

   // free the reply
   dbus_message_unref(reply);
}

/**
 * Server that exposes a method call and waits for it to be called
 */
int dbus_listen()
{
	 DBusMessage* msg;

   // loop, testing for new messages
	 if(conn==NULL)return -1;

      // non blocking read of the next available message
      dbus_connection_read_write(conn, 0);
      msg = dbus_connection_pop_message(conn);

      // loop again if we haven't got a message
      if (NULL == msg)return -1;
      printf("MESSAGE RECIEVED\n");

      // check this is a method call for the right interface & method
      if (dbus_message_is_method_call(msg, "serproxy.method.Type", "Method"))
         reply_to_method_call(msg, conn);

      // free the message
      dbus_message_unref(msg);


}

int dbus_receive_init()
{

	   DBusMessage* reply;
	   DBusMessageIter args;

	   DBusError err;
	   int ret;
	   char* param;

	   printf("Listening for method calls\n");

	   // initialise the error
	   dbus_error_init(&err);

	   // connect to the bus and check for errors
	   conn = dbus_bus_get(DBUS_BUS_SESSION, &err);
	   if (dbus_error_is_set(&err)) {
	      fprintf(stderr, "Connection Error (%s)\n", err.message);
	      dbus_error_free(&err);
	   }
	   if (NULL == conn) {
	      fprintf(stderr, "Connection Null\n");
	      return -1;
	   }

	   // request our name on the bus and check for errors
	   ret = dbus_bus_request_name(conn, "serproxy.method.server", DBUS_NAME_FLAG_REPLACE_EXISTING , &err);
	   if (dbus_error_is_set(&err)) {
	      fprintf(stderr, "Name Error (%s)\n", err.message);
	      dbus_error_free(&err);
	   }
	   if (DBUS_REQUEST_NAME_REPLY_PRIMARY_OWNER != ret) {
	      fprintf(stderr, "Not Primary Owner (%d)\n", ret);
	      return -1;
	   }

	   return 1;

}



/* Main routine for the server threads */
thr_startfunc_t serve_pipe(void *data)
{
	char sio_buf[BUFSIZ], sock_buf[BUFSIZ];
	int fd_max, sio_fd, sock_fd;
	int sio_count, sock_count;
	int res, port,count;
	fd_set rfds, wfds;
	pipe_s *pipe = (pipe_s *)data;
#if defined(__UNIX__)
	struct timeval tv = {pipe->timeout, 0};
	struct timeval *ptv = &tv;
#elif defined(__WIN32__)
	struct timeval tv = {0,10000};
	struct timeval *ptv = &tv;
	DWORD msecs = 0, timeout = pipe->timeout * 1000;
	bool msgs;
#endif
	count=0;
	port = pipe->sio.info.port;

	/* Only proceed if we can lock the mutex */
	if (thr_mutex_trylock(pipe->mutex))
	{
		error("server(%d) - resource is locked", port);
	}
	else
	{

		sio_count = 0;
		sock_count = 0;
		sio_fd = pipe->sio.fd;
		sock_fd = pipe->sock.fd;
#if defined(__UNIX__)
		fd_max = sio_fd > sock_fd ? sio_fd : sock_fd;	
#elif defined(__WIN32__)
		fd_max = sock_fd;
		msecs = GetTickCount();
#endif
		fprintf(stderr, "server(%d) - thread started\n", port);
		
		while (1)
		{

			int count;




			FD_ZERO(&rfds);
			FD_ZERO(&wfds);


#if defined(__UNIX__)
			/* Always ask for read notification to check for EOF */			
			FD_SET(sio_fd, &rfds);
			/* Only ask for write notification if we have something to write */
			if (sock_count > 0)
				FD_SET(sio_fd, &wfds);

			/* Reset timeout values */
			tv.tv_sec = pipe->timeout;
			tv.tv_usec = 0;

#endif
			/* Always ask for read notification to check for EOF */
			FD_SET(sock_fd, &rfds);
			/* Only ask for write notification if we have something to write */
			if (sio_count > 0)
				FD_SET(sock_fd, &wfds);

			//DBG_MSG2("server(%d) waiting for events", port);
			
			/* Wait for read/write events */
			res = select(fd_max + 1, &rfds, &wfds, NULL, ptv);
			if (res == -1)
			{
				perror2("server(%d) - select()", port);
				break;
			}
#if defined(__UNIX__)

			/* Use the select result for timeout detection */
			if (res == 0)
			{
				fprintf(stderr, "server(%d) - timed out\n", port);
				break;
			}

			/* Input from serial port? */
			if (FD_ISSET(sio_fd, &rfds))
#elif defined(__WIN32__)
				
				if (res == 0)fprintf(stderr, "server(%d %d) - timed out\n", port,count++);


			if (1)
#endif
			{
				/* Only read input if buffer is empty */
				if (sio_count == 0)
				{
					sio_count = sio_read(&pipe->sio, sio_buf, sizeof(sio_buf));
					if (sio_count <= 0)
					{
						if (sio_count == 0)
						{
#if defined(__UNIX__)
							fprintf(stderr, "server(%d) - EOF from sio\n", port);
							break;
#endif
						}
						else
						{
							perror2("server(%d) - read(sio)", port);
							break;
						}
					}
					else 
					{
						DBG_MSG3("server(%d) - read %d bytes from sio", port, sio_count);
					}
				}
			}

			/* Write to socket possible? */
			if (FD_ISSET(sock_fd, &wfds))
			{
				if (sio_count > 0)
				{
					if ((res = tcp_write(&pipe->sock, sio_buf, sio_count)) < 0)
					{
						perror2("server(%d) - write(sock)", port);
						break;
					}
					DBG_MSG3("server(%d) - Wrote %d bytes to sock", port, res);
					sio_count -= res;
				}
			}


			/* Input from socket? */
			if (FD_ISSET(sock_fd, &rfds))
			{
				/* Only read input if buffer is empty */
				if (sock_count == 0)
				{
					sock_count = tcp_read(&pipe->sock, sock_buf, sizeof(sock_buf));
					if (sock_count <= 0)
					{
						if (sock_count == 0)
						{
							fprintf(stderr, "server(%d) - EOF from sock\n", port);
							break;
						}
						else
						{
							perror2("server(%d) - read(sock)", port);
							break;
						}
					}
					DBG_MSG3("server(%d) - read %d bytes from sock", port, sock_count);
				}
			}

#if defined(__UNIX__)
			/* Write to serial port possible? */
			if (FD_ISSET(sio_fd, &wfds))
#elif defined(__WIN32__)
			
			/* No socket IO performed? */
			if ((!FD_ISSET(sock_fd, &rfds)) && (!FD_ISSET(sock_fd, &wfds)))
			{
				/* Break on a time out */
				if (GetTickCount() - msecs > timeout)
				{
					fprintf(stderr, "server(%d) - timed out\n", port);
					break;					
				}
			}
			else
			{
				msecs = GetTickCount();
			}

			if (1)
#endif
			{
				if (sock_count > 0)
				{
					if ((res = sio_write(&pipe->sio, sock_buf, sock_count)) < 0)
					{
						perror2("server(%d) - write(sio)", port);
						break;
					}
					DBG_MSG3("server(%d) - wrote %d bytes to sio", port, res);
					sock_count -= res;
				}
			}

		}
		
		/* Unlock our mutex */
		thr_mutex_unlock(pipe->mutex);		
	}

   	fprintf(stderr, "server(%d) exiting\n", port);

	/* Clean up - don't call pipe_cleanup() as that would nuke our mutex */
	sio_cleanup(&pipe->sio);
	tcp_cleanup(&pipe->sock);


	free(pipe);
	
	thr_exit((thr_exitcode_t)0);

	return (thr_exitcode_t)0;
}

void debug(void)
{
	vlist_i *it;
	pipe_s *pit;
	int i = 1;
	
	fprintf(stderr, "pipes:\n\n");
	vlist_debug(&pipes, stderr);

	for (it = pipes.head; it; it = it->next)
	{
		pit = (pipe_s *)it->data;

		fprintf(stderr, "sio[%d]:\n\n", i);
		sio_debug(&pit->sio, stderr);

		fprintf(stderr, "sock[%d]:\n\n", i);
		tcp_debug(&pit->sock, stderr);

		i++;
	}
}
